import axios from "axios";
import dotenv from "dotenv";

dotenv.config();

const WeatherController = async (req, res) => {
    if (req.method === "GET") {
        if (req.path.startsWith("/weather")) {
            const city = req.path.split("/")[2];
            const { WEATHER_API_URL } = process.env;
            const apiURL = WEATHER_API_URL.replace("<<city>>", city);
            const response = await axios.get(apiURL);
            
            if (response.status === 200) {
                const data = response.data;
                if (data.records.length) {
                    res.json({ nhits: data.nhits, parameters: data.parameters, records: data.records, facet_groups: data.facet_groups });
                } else {
                    res.status(404).json({error: "no city found with that name..."});
                }
            } else {
                res.status(500).json({error: "something bad happened..."});
            }
        }
    }
};

export default WeatherController;
