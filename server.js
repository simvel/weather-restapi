import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import WeatherRoutes from "./routes/WeatherRoutes.js";

dotenv.config();

const { SERVER_HOST, SERVER_PORT } = process.env;

const corsOptions = {
    origin: "http://127.0.0.1:5500",
    methods: "GET",
    optionSuccessStatus: 204,
};

const app = express();

app.use(cors(corsOptions));

app.use(WeatherRoutes);

app.listen(SERVER_PORT, SERVER_HOST, () => {
    console.log(`Listening on http://${SERVER_HOST}:${SERVER_PORT}`)
});