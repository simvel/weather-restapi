import express from "express";
import WeatherController from "../controllers/WeatherController.js";

const WeatherRoutes = express.Router();

WeatherRoutes.get("/weather/:city", WeatherController);

export default WeatherRoutes;