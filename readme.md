# Express Meteo API

## création du projet from scratch

### initialisation du projet

Se placer dans le terminal à la racine du projet :

```bash
npm init -y
npm i express axios cors
npm i --save-dev dotenv
mkdir controllers routes
touch .gitignore server.js ./controllers/WeatherController.js ./routes/WeatherRoutes.js .env
echo "node_modules" >> .gitignore
git init
git add .
git commit -m "first commit"
```

En option (je le recommande fortement), on peut installer nodemon pour faciliter le développement (le flag -g l'installera de manière globale sur la machine, en l'enlevant on l'installe uniquement pour le projet en cours) :

```bash
npm i -g nodemon
```

Remplacer le contenu du package.json par ce qui suit :

```json
{
  "name": "express_meteo_api",
  "version": "1.0.0",
  "description": "",
  "main": "server.js",
  "type": "module",
  "scripts": {
    "dev": "nodemon server.js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "axios": "^1.6.5",
    "cors": "^2.8.5",
    "express": "^4.18.2"
  },
  "devDependencies": {
    "dotenv": "^16.3.1"
  }
}

```


Ajouter les lignes suivantes dans le fichier .env :

```bash
SERVER_HOST="127.0.0.1"
SERVER_PORT="3000"

WEATHER_API_URL="https://public.opendatasoft.com/api/records/1.0/search/?dataset=donnees-synop-essentielles-omm&q=<<city>>&facet=nom&facet=region&facet=dept&facet=flux"
```



### WeatherController.js

Dans le fichier WeatherController.js, importer les dépendances et lancer la méthode dotenv.config() :

```js
import axios from "axios";
import dotenv from "dotenv";

dotenv.config();
```

Créer une constante WeatherController qui contiendra une méthode asynchrone et l'exporter par défaut :

```js
const WeatherController = async (req, res) => {};

export default WeatherController;
```

Dans la méthode WeatherController, vérifier que la requête est en méthode GET et que l'URI correspond à "/weather" :

```js
const WeatherController = async (req, res) => {
    if (req.method === "GET") {
        if (req.path.startsWith("/weather")) {

        }
    }
};
```

Récupérer le nom de la ville dans l'URI et l'insérer dans le endpoint de l'API météo :

```js
const WeatherController = async (req, res) => {
    if (req.method === "GET") {
        if (req.path.startsWith("/weather")) {
            const city = req.path.split("/")[2];
            const { WEATHER_API_URL } = process.env;
            const apiURL = WEATHER_API_URL.replace("<<city>>", city);
        }
    }
};
```

Faire la requête vers l'API avec axios (ne pas oublier le await) :

```js
const WeatherController = async (req, res) => {
    if (req.method === "GET") {
        if (req.path.startsWith("/weather")) {
            const city = req.path.split("/")[2];
            const { WEATHER_API_URL } = process.env;
            const apiURL = WEATHER_API_URL.replace("<<city>>", city);
            const response = await axios.get(apiURL);
        }
    }
};
```

Traiter la réponse de l'API :

```js
const WeatherController = async (req, res) => {
    if (req.method === "GET") {
        if (req.path.startsWith("/weather")) {
            const city = req.path.split("/")[2];
            const { WEATHER_API_URL } = process.env;
            const apiURL = WEATHER_API_URL.replace("<<city>>", city);
            const response = await axios.get(apiURL);
            
            if (response.status === 200) {
                const data = response.data;
                if (data.records.length) {
                    res.json({ nhits: data.nhits, parameters: data.parameters, records: data.records, facet_groups: data.facet_groups });
                } else {
                    res.status(404).json({error: "no city found with that name..."});
                }
            } else {
                res.status(500).json({error: "something bad happened..."});
            }
        }
    }
};
```

Si le statut de la réponse est 200, on extrait les données dans la constante data. Si le tableau data.records contient quelque chose, on retourne les data dans un json (le statut est automatiquement défini à 200), sinon on retourne une erreur avec un statut 404.
Si le statut de la requête axios est différent de 200, on retourne une erreur 500.

### WeatherRoutes.js

Dans le fichier WeatherRoutes.js, importer les dépendances :

```js
import express from "express";
import WeatherController from "../controllers/WeatherController.js";
```

Dans une constance WeatherRoutes, récupérer la méthode Router() de express :

```js
const WeatherRoutes = express.Router();
```

Associer le WeatherController à WeatherRoutes.get avec l'URI "/weather/:city" :

```js
WeatherRoutes.get("/weather/:city", WeatherController);
```

Exporter WeatherRoutes par défaut :

```js
export default WeatherRoutes;
```

### server.js

Dans le fichier server.js, importer les dépendances :

```js
import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import WeatherRoutes from "./routes/WeatherRoutes.js";
```

Appeler la méthode config() de dotenv afin d'accéder aux variables d'environnement situées dans le fichier .env et appeler les variables SERVER_HOST et SERVER_PORT depuis process.env.

```js
dotenv.config();

const { SERVER_HOST, SERVER_PORT } = process.env;
```

Déclarer une constante corsOptions qui contiendra un objet. À l'intérieur de celui-ci, spécifier l'origine des requêtes clients autorisées, les méthodes autorisées et le statut des par défaut des requêtes acceptées :

```js
const corsOptions = {
    origin: "http://127.0.0.1:5500",
    methods: "GET",
    optionSuccessStatus: 204,
};
```

Dans une constante app, instancier l'application express :

```js
const app = express();
```

Associer cors à l'app :

```js
app.use(cors(corsOptions));
```

Associer les routes à l'app :

```js
app.use(WeatherRoutes);
```

Faire écouter l'app sur le port et l'hôte spécifiés dans le .env :

```js
app.listen(SERVER_PORT, SERVER_HOST, () => {
    console.log(`Listening on http://${SERVER_HOST}:${SERVER_PORT}`)
});
```

## Utilisation à partir du repo existant

Installer les dépendances du projet en se plaçant à la racine du projet dans un terminal :

```bash
npm i
```

Puis démarrer l'API sur le serveur de développement, toujours dans un terminal à la racine du projet :

```bash
npm run dev
```

Les requêtes doivent être envoyées à l'URL `http://127.0.0.1:3000/weather/<<nom_de_la_ville>>`. Si la ville correspond à un bulletin météo, une réponse sera renvoyée sous la forme d'un json de type :

```json
{
    "nhits": 40909,
    "parameters": {
        //paramêtres de la requête...
    },
    "records": [
        //bulletin météo dont un champ record_timestamp
    ],
    "facet_groups": [
        //métadonnées du bulletin
    ]
}
